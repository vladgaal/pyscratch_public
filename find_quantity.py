import os


# def find(nome, formato):
#     a = 0
#     while True:
#         if (a + 1) >= 100:
#             try:
#                 fn = open(nome + '' + str(a + 1) + formato)
#                 fn.close()
#             except FileNotFoundError:
#                 return a
#
#         if (a + 1) >= 10 and (a + 1) < 100:
#             try:
#                 fn = open(nome + '0' + str(a + 1) + formato)
#                 fn.close()
#             except FileNotFoundError:
#                 return a
#
#         if (a + 1) < 10:
#             try:
#                 fn = open(nome + '00' + str(a + 1) + formato)
#                 fn.close()
#             except FileNotFoundError:
#                 return a
#         a = a+1


def find(dir, name):
    qtd = 0
    #os.chdir(dir)
    for root, dirs, files in os.walk(dir):
        if root == dir:
            for filename in files:
                if name in filename:
                    qtd = qtd + 1
    return qtd


if __name__ == '__main__':
    nome = 'A1_03_1_1_Phase Contrast_'
    dir = "D:/GDrive/Fer/170804_174239_Plate 1/"
    formato = '.tif'

    quantidade = find(dir, nome)

    print('quantidade: {}'.format(quantidade))
