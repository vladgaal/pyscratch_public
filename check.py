import cv2
import numpy as np
import os
import process_contours as proc


def contourIntersect(original_image, contour1, contour2):


    # Create image filled with zeros the same size of original image
    blank = np.zeros(original_image.shape[0:2])

    # Copy each contour into its own image and fill it with '1'
    image3 = cv2.fillPoly(blank.copy(), pts=[contour1], color=(255, 255, 255))
    image4 = cv2.fillPoly(blank.copy(), pts=[contour2], color=(255, 255, 255))

    # Use the logical AND operation on the two images
    # Since the two images had bitwise AND applied to it,
    # there should be a '1' or 'True' where there was intersection
    # and a '0' or 'False' where it didnt intersect
    intersection = np.logical_and(image3, image4)
    # Check if there was a '1' in the intersection array

    return intersection.any()


if __name__ == '__main__':
    path = 'D:\\GDrive\\Fer\\170804_174239_Plate 1\\'
    nome = 'A1_03_1_1_Phase Contrast_022'
    formato = '.tif'
    para_verificar = [77, 129]
    corte = 2
    dir = os.path.abspath(path + nome + formato)
    im = cv2.imread(dir)
    contours = proc.treatment(im, corte)
    resultado = contourIntersect(im, contours[para_verificar[0]], contours[para_verificar[-1]])
    print(resultado)
