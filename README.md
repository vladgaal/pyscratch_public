# PyScrath
> Python based program that automates the analysis of biological assays. 

---

## Clone the repository

> If you don't yet have, [download and install git](https://git-scm.com/downloads). Use the git clone command to clone from: https://bitbucket.org/vladgaal/pyscratch_public.git .

---

## Install dependencies
**Windows users**

Download [here](https://www.python.org/downloads/windows/) and install the latest version of Python 3.7.
Go to the cloned folder ".\Dependencies" and execute the "install_all.bat" (just for x64 computers) to install everything that you need to run.

**Linux users**

Install/update the following libraries:

> ```
> apt-get update && apt-get install -y --no-install-recommends \
>       "python3-matplotlib=2.1.1-2ubuntu3" \
>       "python3-numpy=1:1.13.3-2ubuntu1" \
>       "python3-opencv=3.2.0+dfsg-4ubuntu0.1" \
>       "python3-pandas=0.22.0-4" \
>       "python3-pyside=1.2.2+source1-3"
> ```

> ```
> sudo -H pip3 install pyside2
> ```

---
## Run

Just execute "Pyscratch.bat" or from the command line (on the cloned folder):

> ```
> python main.py
> ```

If you want to run an older version just use the respective "mainVx.py", like:

> ```
> python mainV4.py
> ```

You can also right-click in the 'Pyscratch.bat' file and 'Send to' -> 'Desktop' to create a shortcut.

---

## Important tips

Based on user experiences, we outlined a set of tips on how to get better results with PyScrath:

> 1 - The file name should follow a regular expression, like: "your_label_001.tif". It is important to have a underline before the sequential numbers just followed by the file format.
> 
> 2 - The files should have sequential numbers: "your_label_001.tif", "your_label_002.tif", "your_label_003.tif" and so on.
> 
> 3 - To improve the results it is highly recommended to acquire the images using phase contrast objectives.
> 
> 4 - If the images have already been acquired with a brightfield objective, it is possible to post-process the images to enhance the edges. For further information visit our guide section [here](http://www.nanocell.com.br/?lan=en)

---

## Release control

V0 -> Initial release;

V1 -> User interface implementation;

V2 -> Improved mathematical method;

V3 -> Changes on the opening images method; 

V4 -> Improved the gap area count;

V5 -> New time input method; Gui is now resizable; Bug fix on "image_processing"; Upgrade to PySide2 + Qt5; Improvements on plot and figure interaction with GUI

V5.1 -> Bug fix on "image_processing"; Detection of file existence;

