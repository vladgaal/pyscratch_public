﻿import sys
import os
import cv2
import matplotlib
matplotlib.use('Agg')
import matplotlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import (QCoreApplication, QPropertyAnimation, QDate, QDateTime, QMetaObject, QObject, QPoint, QRect, QSize, QTime, QUrl, Qt, QEvent, QThread, Signal)
from PySide2.QtGui import (QImage, QBrush, QColor, QConicalGradient, QCursor, QFont, QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtCore import QFile, QObject, Signal, Slot, QTimer
from PySide2.QtWidgets import QApplication, QVBoxLayout, QWidget
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *
import find_quantity
import normalizer
import image_processing
# GUI MAIN
from ui_files import mainDialogV3 as mainDialog
# GUI SPLASH SCREEN
from ui_files.splash_screen import Ui_SplashScreen
import pandas as pd
import time
import copy
import random


__appname__ = "PyScratch"
__version__ = "V5.1"
__cut__ = 2
control = 0


class DSL(QObject):
    dataChanged = Signal(list)

    def __init__(self, parent=None):
        # LOAD HMI
        super().__init__(parent)
        designer_file = QFile("userInterface.ui")
        if designer_file.open(QFile.ReadOnly):
            loader = QUiLoader()
            self.ui = loader.load(designer_file)
            designer_file.close()
            self.ui.show()
        # Data to be visualized
        self.data = []

    def mainLoop(self):
        self.data = []
        for i in range(10):
            self.data.append(random.randint(0, 10))
        # Send data to graph
        self.dataChanged.emit(self.data)
        # LOOP repeater
        QTimer.singleShot(10, self.mainLoop)


class MySignal(QObject):
    tempo = Signal(float, float, int)
    finished = Signal(str)
    changePixmap = Signal(QImage)


class WorkThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.signal = MySignal()
        self.formato = '.tif'
        self.quantidade = 0
        self.areaOut = 0
        self.tempoOut = 0
        self.corte = 0
        self.nome = ''
        self.path = ''
        self.encerrar = False
        self.tempo = 0

    def run(self):
        start_time = time.time()
        for c in range(self.quantidade):
            if not self.encerrar:
                area, image = image_processing.process_a_picture2(self.path, (self.nome + str(c + 1).zfill(len(str(self.quantidade)))),
                                                                  self.formato, self.corte, passImage=True)
                self.signal.tempo.emit(c * self.tempo, area, c)
                h, w, ch = image.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(image.data, w, h, bytesPerLine, QImage.Format_RGB888)
                self.signal.changePixmap.emit(convertToQtFormat)
                time.sleep(0.1)

            else:
                time.sleep(0.5)
                break
        print("Elapsed time: %s seconds ---" % (time.time() - start_time))
        time.sleep(1)
        if not self.encerrar:
            # print("OK")
            self.signal.finished.emit('OK')
        else:
            self.signal.finished.emit('STOPPED')


class MainDialog(QDialog, mainDialog.Ui_Dialog):
    print("Loading PyScratch....")

    tempoVec = []
    areaVec = []
    normalizadoVec = []
    folder = '.'

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(__appname__ + ' - ' + __version__)
        self.setWindowFlag(Qt.WindowMinimizeButtonHint, True)
        self.setWindowFlag(Qt.WindowMaximizeButtonHint, True)

        global control

        self.analisarThread = WorkThread()
        self.analisarThread.signal.tempo.connect(self.timeStep)
        self.analisarThread.signal.finished.connect(self.doneAnalyze)
        self.analisarThread.signal.changePixmap.connect(self.setImage)

        self.progressBar.setValue(0)
        self.arquivoLine.setEnabled(False)
        self.pararButton.setEnabled(False)
        self.analisarButton.setEnabled(False)
        self.salvarButton.setEnabled(False)

        self.arquivoButton.clicked.connect(self.load)
        self.salvarButton.clicked.connect(self.save)
        self.analisarButton.clicked.connect(self.analyze)
        self.sairButton.clicked.connect(self.close)
        self.pararButton.clicked.connect(self.shutDown)

        self.statusLabel.setText("Holding")
        self.statusLabel.setStyleSheet("QLabel#statusLabel {color: white; font: bold 12pt; " +
                                         "font-family: Tahoma;}")

        self.timeExp.addItems(['MINUTES', 'HOURS'])

        self.setPlots()
        self.setAxis()

        control = 2

    ########################################################################
    ## START - PLOT SETTINGS
    ########################################################################
    def setPlots(self):
        ## COMMON
        color1 = 'black'
        font_mid = {'family': 'Segoe UI', 'size': 16}
        font_big = {'family': 'Segoe UI', 'weight': 'bold', 'size': 22}
        matplotlib.rc('font', **font_mid)

        ## PLOT
        self.fig = Figure(figsize=(7, 5), dpi=65, frameon=True)
        self.fig.subplots_adjust(left=0.15, bottom=0.12, right=0.98, top=0.98)
        self.ax = self.fig.add_subplot(111)
        self.canvas = FigureCanvas(self.fig)
        self.plot_area.addWidget(self.canvas)

    def setAxis(self):
        font_mid = {'family': 'Segoe UI', 'size': 16}
        font_big = {'family': 'Segoe UI', 'weight': 'bold', 'size': 22}
        color1 = 'black'
        legend_elements = [Line2D([0], [0], marker='o', color='w', label='Area', markerfacecolor='r', markersize=10)]

        self.ax.patch.set_alpha(0)
        for spine in self.ax.spines.values():
            spine.set_edgecolor(color1)
        self.ax.xaxis.label.set_color(color1)
        self.ax.yaxis.label.set_color(color1)
        self.ax.tick_params(axis='both', colors=color1)
        self.ax.grid(color='black', alpha=0.5, linestyle='dashed', linewidth=0.5)
        self.ax.set_xlabel("Time [min]", **font_big)
        self.ax.set_ylabel("Area [$pixels^2$]", **font_big)
        self.ax.legend(handles=legend_elements, loc='best')

    @Slot(QImage)
    def setImage(self, image):
        frame = image.scaledToHeight(self.label_image.height())
        self.label_image.setPixmap(QPixmap.fromImage(frame))

    def imageSample(self, file):
        sample = cv2.imread(file)
        h, w, ch = sample.shape
        bytesPerLine = ch * w
        convertToQtFormat = QImage(sample.data, w, h, bytesPerLine, QImage.Format_RGB888)
        frame = convertToQtFormat.scaledToHeight(self.label_image.height())
        self.label_image.setPixmap(QPixmap.fromImage(frame))

    def shutDown(self):
        self.analisarThread.encerrar = True

    @Slot(str)
    def doneAnalyze(self, msg):
        # print("aqui")
        self.analisarThread.encerrar = False
        self.normalizadoVec = normalizer.normalize(self.areaVec)
        self.dados = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dados['Time (min)'] = self.tempoVec
        self.dados['Area (pixel^2)'] = self.areaVec
        self.dados['Normalized '] = self.normalizadoVec
        self.pararButton.setEnabled(False)
        self.salvarButton.setEnabled(True)
        self.analisarButton.setEnabled(True)

        if msg == "OK":
            print(msg)
            self.statusLabel.setText("Finished")
            self.statusLabel.setStyleSheet("QLabel#statusLabel {color: yellow; font: bold 12pt; " +
                                           "font-family: Tahoma;}")
            self.progressBar.setValue(100)
        elif msg == "STOPPED":
            print(msg)
            self.statusLabel.setText("Stopped")
            self.statusLabel.setStyleSheet("QLabel#statusLabel {color: red; font: bold 12pt; " +
                                           "font-family: Tahoma;}")

    def updatePlot(self, data):
        self.ax.plot(data[0], data[1], 'ro', markersize=7)
        self.canvas.draw()

    @Slot(float, float, int)
    def timeStep(self, tempo, area, atual):
        self.tempoVec.append(tempo)
        self.areaVec.append(area)
        self.updatePlot([self.tempoVec, self.areaVec])
        self.progressBar.setValue(100*atual/self.analisarThread.quantidade)

    def analyze(self):
        self.ax.cla()
        self.setAxis()
        del self.tempoVec[:]
        del self.areaVec[:]
        self.analisarThread.encerrar = False
        self.analisarThread.corte = float(__cut__)
        self.analisarThread.nome = self.inNome
        self.analisarThread.path = self.inDir
        self.analisarThread.formato = self.extension
        self.analisarThread.tempo = float(self.timeNum.text())*(60 if self.timeExp.currentIndex() == 1 else 1)
        self.analisarThread.start()
        self.statusLabel.setText("Processing...")
        self.statusLabel.setStyleSheet("QLabel#statusLabel {color: red; font: bold 12pt; " +
                                         "font-family: Tahoma;}")
        self.pararButton.setEnabled(True)
        self.analisarButton.setEnabled(False)

    def load(self):
        dir = self.folder
        self.inFile = QFileDialog.getOpenFileName(self, __appname__, dir=dir,
                                                  filter="TIF image files (*.tif *.tiff);;Images (*.png *.jpg)")
        if self.inFile[0] != '':
            self.inNome = QtCore.QDir(self.inFile[0]).dirName()[:(len(QtCore.QDir(self.inFile[0]).dirName()))]
            self.inDir = QtCore.QDir(self.inFile[0]).path()[:(len(QtCore.QDir(self.inFile[0]).path())-len(QtCore.QDir(self.inFile[0]).dirName()))]
            self.extension = '.' + self.inNome.split(".")[-1]
            self.inNome = '.'.join(self.inNome.split(".")[:-1])
            self.inNome = '_'.join(self.inNome.split("_")[:-1]) + "_"
            print("Directory: ", self.inDir)
            print("Name: ", self.inNome)
            print("Extension: ", self.extension)
            self.arquivoLine.setText(self.inNome)
            self.analisarThread.quantidade = find_quantity.find(self.inDir, self.inNome)
            self.quantidadeLabel.setText(str(self.analisarThread.quantidade))
            file_name = self.inDir + self.inNome + str(1).zfill(len(str(self.analisarThread.quantidade))) + self.extension

            try:
                teste_file = open(file_name)
                teste_file.close()
                self.statusLabel.setText("Ready")
                self.statusLabel.setStyleSheet(
                    "QLabel#statusLabel {color: green; font: bold 12pt; font-family: Tahoma;}")
                self.analisarButton.setEnabled(True)
                self.imageSample(file_name)
                self.folder = os.path.dirname(os.path.abspath(self.inFile[0]))
            except IOError:
                print("File not accessible")
                result = QMessageBox.critical(self, __appname__, "The file: " + file_name +
                                              "\ndoes not exist or is not accessible", QMessageBox.Abort)

    def save(self):
        dir = self.folder
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")

        if self.outFile[0] != '':
            self.dados.to_csv(self.outFile[0][:len(self.outFile[0])-4] + '_data.csv', sep='\t', index=False)
            self.fig.savefig(self.outFile[0][:len(self.outFile[0])-4]+'_plot.png')

    def closeEvent(self, event, *args, **kwargs):
        """Ritual de encerramento"""
        result = QMessageBox.question(self, __appname__, "Are you sure you want to quit?",
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        if result == QMessageBox.Yes:
            """Colocar aqui tudo que tiver que ser feito antes de sair"""
            self.analisarThread.encerrar = True
            time.sleep(1)
            event.accept()
        else:
            event.ignore()


# SPLASH SCREEN
class SplashScreen(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_SplashScreen()
        self.ui.setupUi(self)

        # QMainWindow.__init__(self)
        # self.ui = Ui_SplashScreen()
        # self.ui.setupUi(self)

        ## UI ==> INTERFACE CODES
        ########################################################################

        ## REMOVE TITLE BAR
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)


        ## DROP SHADOW EFFECT
        self.shadow = QGraphicsDropShadowEffect(self)
        self.shadow.setBlurRadius(20)
        self.shadow.setXOffset(0)
        self.shadow.setYOffset(0)
        self.shadow.setColor(QColor(0, 0, 0, 60))
        self.ui.dropShadowFrame.setGraphicsEffect(self.shadow)

        ## QTIMER ==> START
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.progress)
        # TIMER IN MILLISECONDS
        self.timer.start(1000)

        self.timer2 = QtCore.QTimer()
        self.timer2.timeout.connect(self.dots)
        # TIMER IN MILLISECONDS
        self.timer2.start(300)

        ## SHOW ==> MAIN WINDOW
        ########################################################################
        self.show()
        ## ==> END ##

    ## ==> APP FUNCTIONS
    ########################################################################

    def runMain(self):
        #pass
        self.dsl = DSL()
        self.dsl.mainLoop()

        self.main = MainDialog()
        self.main.show()

        #self.dsl.dataChanged.connect(self.main.update_plot)

    def dots(self):
        # ANIMATE DOTS
        if self.ui.label_loading.text() == "loading.":
            self.ui.label_loading.setText("loading..")
        elif self.ui.label_loading.text() == "loading..":
            self.ui.label_loading.setText("loading...")
        elif self.ui.label_loading.text() == "loading...":
            self.ui.label_loading.setText("loading.")

    def progress(self):

        global control

        # OPEN APP
        if control == 0:
            #self.run_main_thread.start()
            self.runMain()

        # WAIT COMMAND TO CLOSE SPLASH SCREEN
        if control == 2:
            # STOP TIMER
            self.timer.stop()
            # CLOSE SPLASH SCREEN
            self.close()


if __name__ == '__main__':
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("Vladimir Gaal")
    QCoreApplication.setOrganizationDomain("")

    app = QApplication(sys.argv)
    window = SplashScreen()
    sys.exit(app.exec_())

