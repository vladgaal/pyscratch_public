# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainDialogV3.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import icones2_rc

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(1150, 600)
        Dialog.setMinimumSize(QSize(1150, 600))
        icon = QIcon()
        icon.addFile(u":/Main/icons/Scratch.png", QSize(), QIcon.Normal, QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet(u"QDialog{\n"
"background-color: qlineargradient(spread:reflect, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(90, 7, 90, 255), stop:0.372881 rgba(26, 23, 99, 252), stop:0.672316 rgba(26, 23, 99, 253), stop:1 rgba(0, 125, 125, 255));\n"
"}\n"
"\n"
"QLabel{\n"
"color: White;\n"
"}\n"
"\n"
"QGroupBox{\n"
"border: 2px solid rgb(190, 190, 190);\n"
"border-radius: 10px;\n"
"margin-top: 3ex\n"
"}\n"
"\n"
"QGroupBox::title{\n"
"subcontrol-origin: margin;\n"
"subcontrol-position: top left;\n"
"padding: 0 3px;\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QToolButton{\n"
"background-color: Transparent; \n"
"color: White;\n"
"border: 2px ridge White;\n"
"border-radius: 15px;\n"
"}\n"
"\n"
"QToolButton#arquivoButton{\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton:hover{\n"
"background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0.215909 rgba(0, 0, 0, 0), stop:1 rgba(15, 243, 255, 255));\n"
"}\n"
"\n"
"QSpinBox { \n"
"border: 2px solid  white; \n"
"border-radius: 5px; \n"
"color: White;\n"
"background-"
                        "color: transparent;\n"
"}\n"
"\n"
"QDoubleSpinBox { \n"
"border: 2px solid  white; \n"
"border-radius: 5px; \n"
"color: White;\n"
"background-color: transparent;\n"
"}\n"
"\n"
"QDoubleSpinBox::up-button { \n"
"background-color: Transparent;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QDoubleSpinBox::up-button:pressed{ \n"
"background-color: white;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QDoubleSpinBox::down-button{ \n"
"background-color: Transparent;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QDoubleSpinBox::down-button:pressed{ \n"
"background-color: white;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QDoubleSpinBox::up-arrow{ \n"
"background-color: transparent;\n"
"border-left: 7px solid none;\n"
"border-right: 7px solid none;\n"
"border-bottom: 6px solid white;\n"
"width: 0px; \n"
"height: 0px; \n"
"}\n"
"\n"
"QDoubleSpinBox::down-arrow{ \n"
"border-left: 7px solid none;\n"
"border-right: 7px solid none;\n"
"border-top: 6px solid white;\n"
"width: 0px; \n"
"height: 0px; \n"
""
                        "}\n"
"\n"
"\n"
"QSpinBox::up-button { \n"
"background-color: Transparent;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QSpinBox::up-button:pressed{ \n"
"background-color: white;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QSpinBox::down-button{ \n"
"background-color: Transparent;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QSpinBox::down-button:pressed{ \n"
"background-color: white;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QSpinBox::up-arrow{ \n"
"background-color: transparent;\n"
"border-left: 7px solid none;\n"
"border-right: 7px solid none;\n"
"border-bottom: 6px solid white;\n"
"width: 0px; \n"
"height: 0px; \n"
"}\n"
"\n"
"QSpinBox::down-arrow{ \n"
"border-left: 7px solid none;\n"
"border-right: 7px solid none;\n"
"border-top: 6px solid white;\n"
"width: 0px; \n"
"height: 0px; \n"
"}\n"
"\n"
"QProgressBar {\n"
"	background-color: rgba(98, 114, 164, 0.3);\n"
"	color: rgb(210, 210, 210);\n"
"	border-style: none;\n"
"	border-radius: 10px;\n"
"	text-align: center;\n"
"}\n"
"QProg"
                        "ressBar::chunk{\n"
"	border-radius: 10px;\n"
"	background-color: qlineargradient(spread:pad, x1:0, y1:0.511364, x2:1, y2:0.523, stop:0 rgba(254, 121, 199, 255), stop:1 rgba(170, 85, 255, 255));\n"
"}\n"
"\n"
"/* COMBOBOX */\n"
"QComboBox{\n"
"	color: White;\n"
"	background-color: transparent;\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(255, 255, 255);\n"
"	padding: 3px;\n"
"	padding-left: 10px;\n"
"}\n"
"QComboBox:hover{\n"
"	border: 2px solid rgb(125, 125, 125);\n"
"}\n"
"QComboBox::drop-down {\n"
"	subcontrol-origin: padding;\n"
"	subcontrol-position: top right;\n"
"	width: 25px; \n"
"	border-left-width: 3px;\n"
"	border-left-color: rgba(255, 255, 255, 0.8);\n"
"	border-left-style: solid;\n"
"	border-top-right-radius: 3px;\n"
"	border-bottom-right-radius: 3px;	\n"
"	background-image: url(:/Main/icons/arrow-bottom.png);\n"
"	background-position: center;\n"
"	background-repeat: no-reperat;\n"
" }\n"
"QComboBox QAbstractItemView {\n"
"	background-color:  rgb(255, 255, 255);\n"
"	padding: 3px;\n"
"	borde"
                        "r-radius: 5px;\n"
"    border: 2px solid  rgb(220, 220, 220);\n"
"    selection-background-color:  rgb(125, 125, 125);\n"
"}\n"
"\n"
"/* LINE EDIT */\n"
"QLineEdit {\n"
"	background-color: rgb(255, 255, 255);\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(210, 210, 210);\n"
"}\n"
"QLineEdit:hover {\n"
"	border: 2px solid rgb(125, 125, 125);\n"
"}\n"
"QLineEdit:focus {\n"
"	border: 2px solid rgb(91, 101, 124);\n"
"}")
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(4, 0, 4, 0)
        self.frame_name = QFrame(Dialog)
        self.frame_name.setObjectName(u"frame_name")
        self.frame_name.setMaximumSize(QSize(16777215, 60))
        self.frame_name.setFrameShape(QFrame.StyledPanel)
        self.frame_name.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.frame_name)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.label = QLabel(self.frame_name)
        self.label.setObjectName(u"label")
        self.label.setMaximumSize(QSize(16777215, 100))
        font = QFont()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout_5.addWidget(self.label)


        self.verticalLayout.addWidget(self.frame_name)

        self.frame_prints = QFrame(Dialog)
        self.frame_prints.setObjectName(u"frame_prints")
        self.frame_prints.setFrameShape(QFrame.StyledPanel)
        self.frame_prints.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.frame_prints)
        self.horizontalLayout_3.setSpacing(6)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.label_image = QLabel(self.frame_prints)
        self.label_image.setObjectName(u"label_image")
        self.label_image.setMinimumSize(QSize(0, 400))
        self.label_image.setMaximumSize(QSize(16777215, 600))
        font1 = QFont()
        font1.setFamily(u"Tahoma")
        font1.setPointSize(20)
        self.label_image.setFont(font1)
        self.label_image.setFrameShape(QFrame.NoFrame)
        self.label_image.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_3.addWidget(self.label_image)

        self.line_2 = QFrame(self.frame_prints)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.VLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout_3.addWidget(self.line_2)

        self.frame = QFrame(self.frame_prints)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_7 = QVBoxLayout(self.frame)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.plot_area = QVBoxLayout()
        self.plot_area.setObjectName(u"plot_area")

        self.verticalLayout_7.addLayout(self.plot_area)


        self.horizontalLayout_3.addWidget(self.frame)


        self.verticalLayout.addWidget(self.frame_prints)

        self.frame_settings = QFrame(Dialog)
        self.frame_settings.setObjectName(u"frame_settings")
        self.frame_settings.setMinimumSize(QSize(0, 110))
        self.frame_settings.setMaximumSize(QSize(16777215, 110))
        self.frame_settings.setFrameShape(QFrame.StyledPanel)
        self.frame_settings.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame_settings)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_input = QFrame(self.frame_settings)
        self.frame_input.setObjectName(u"frame_input")
        self.frame_input.setMinimumSize(QSize(590, 0))
        self.frame_input.setMaximumSize(QSize(590, 16777215))
        self.frame_input.setFrameShape(QFrame.StyledPanel)
        self.frame_input.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frame_input)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.groupBox_2 = QGroupBox(self.frame_input)
        self.groupBox_2.setObjectName(u"groupBox_2")
        font2 = QFont()
        font2.setFamily(u"Tahoma")
        font2.setPointSize(12)
        self.groupBox_2.setFont(font2)
        self.verticalLayout_8 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(4, 4, 4, 4)
        self.label_2 = QLabel(self.groupBox_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font2)

        self.verticalLayout_8.addWidget(self.label_2)

        self.frame_3 = QFrame(self.groupBox_2)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.frame_3)
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.arquivoLine = QLineEdit(self.frame_3)
        self.arquivoLine.setObjectName(u"arquivoLine")
        font3 = QFont()
        font3.setFamily(u"Tahoma")
        font3.setPointSize(10)
        self.arquivoLine.setFont(font3)

        self.horizontalLayout_4.addWidget(self.arquivoLine)

        self.arquivoButton = QToolButton(self.frame_3)
        self.arquivoButton.setObjectName(u"arquivoButton")

        self.horizontalLayout_4.addWidget(self.arquivoButton)


        self.verticalLayout_8.addWidget(self.frame_3)

        self.frame_2 = QFrame(self.groupBox_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.frame_2)
        self.horizontalLayout_5.setSpacing(6)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.label_4 = QLabel(self.frame_2)
        self.label_4.setObjectName(u"label_4")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setFont(font2)

        self.horizontalLayout_5.addWidget(self.label_4)

        self.timeNum = QLineEdit(self.frame_2)
        self.timeNum.setObjectName(u"timeNum")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.timeNum.sizePolicy().hasHeightForWidth())
        self.timeNum.setSizePolicy(sizePolicy1)
        self.timeNum.setMinimumSize(QSize(50, 0))
        self.timeNum.setMaximumSize(QSize(150, 16777215))
        self.timeNum.setFont(font3)
        self.timeNum.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_5.addWidget(self.timeNum)

        self.timeExp = QComboBox(self.frame_2)
        self.timeExp.setObjectName(u"timeExp")
        self.timeExp.setMinimumSize(QSize(130, 0))
        self.timeExp.setFont(font3)

        self.horizontalLayout_5.addWidget(self.timeExp)

        self.line = QFrame(self.frame_2)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout_5.addWidget(self.line)

        self.label_5 = QLabel(self.frame_2)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMinimumSize(QSize(150, 0))
        self.label_5.setFont(font2)
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_5.addWidget(self.label_5)

        self.quantidadeLabel = QLabel(self.frame_2)
        self.quantidadeLabel.setObjectName(u"quantidadeLabel")
        self.quantidadeLabel.setMinimumSize(QSize(50, 0))
        self.quantidadeLabel.setFont(font2)

        self.horizontalLayout_5.addWidget(self.quantidadeLabel)


        self.verticalLayout_8.addWidget(self.frame_2)


        self.verticalLayout_4.addWidget(self.groupBox_2)


        self.horizontalLayout.addWidget(self.frame_input)

        self.frame_status = QFrame(self.frame_settings)
        self.frame_status.setObjectName(u"frame_status")
        self.frame_status.setMinimumSize(QSize(165, 0))
        self.frame_status.setFrameShape(QFrame.StyledPanel)
        self.frame_status.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frame_status)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.groupBox = QGroupBox(self.frame_status)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setFont(font2)
        self.verticalLayout_6 = QVBoxLayout(self.groupBox)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.frame_group = QFrame(self.groupBox)
        self.frame_group.setObjectName(u"frame_group")
        self.frame_group.setFrameShape(QFrame.StyledPanel)
        self.frame_group.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.frame_group)
        self.gridLayout.setObjectName(u"gridLayout")
        self.progressBar = QProgressBar(self.frame_group)
        self.progressBar.setObjectName(u"progressBar")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.progressBar.sizePolicy().hasHeightForWidth())
        self.progressBar.setSizePolicy(sizePolicy2)
        self.progressBar.setValue(24)

        self.gridLayout.addWidget(self.progressBar, 3, 0, 1, 2)

        self.statusLabel = QLabel(self.frame_group)
        self.statusLabel.setObjectName(u"statusLabel")
        self.statusLabel.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.statusLabel, 0, 0, 1, 2)


        self.verticalLayout_6.addWidget(self.frame_group)


        self.verticalLayout_3.addWidget(self.groupBox)


        self.horizontalLayout.addWidget(self.frame_status)

        self.frame_buttons = QFrame(self.frame_settings)
        self.frame_buttons.setObjectName(u"frame_buttons")
        self.frame_buttons.setMinimumSize(QSize(380, 0))
        self.frame_buttons.setMaximumSize(QSize(380, 16777215))
        self.frame_buttons.setFrameShape(QFrame.StyledPanel)
        self.frame_buttons.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_buttons)
        self.horizontalLayout_2.setSpacing(4)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.analisarButton = QToolButton(self.frame_buttons)
        self.analisarButton.setObjectName(u"analisarButton")
        self.analisarButton.setMinimumSize(QSize(90, 90))
        self.analisarButton.setFont(font2)
        icon1 = QIcon()
        icon1.addFile(u":/Main/icons/startB.png", QSize(), QIcon.Normal, QIcon.Off)
        self.analisarButton.setIcon(icon1)
        self.analisarButton.setIconSize(QSize(64, 64))
        self.analisarButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_2.addWidget(self.analisarButton)

        self.pararButton = QToolButton(self.frame_buttons)
        self.pararButton.setObjectName(u"pararButton")
        self.pararButton.setMinimumSize(QSize(90, 90))
        self.pararButton.setFont(font2)
        icon2 = QIcon()
        icon2.addFile(u":/Main/icons/stopB.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pararButton.setIcon(icon2)
        self.pararButton.setIconSize(QSize(64, 64))
        self.pararButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_2.addWidget(self.pararButton)

        self.salvarButton = QToolButton(self.frame_buttons)
        self.salvarButton.setObjectName(u"salvarButton")
        self.salvarButton.setMinimumSize(QSize(90, 90))
        self.salvarButton.setFont(font2)
        icon3 = QIcon()
        icon3.addFile(u":/Main/icons/salvarB.png", QSize(), QIcon.Normal, QIcon.Off)
        self.salvarButton.setIcon(icon3)
        self.salvarButton.setIconSize(QSize(64, 64))
        self.salvarButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_2.addWidget(self.salvarButton)

        self.sairButton = QToolButton(self.frame_buttons)
        self.sairButton.setObjectName(u"sairButton")
        self.sairButton.setMinimumSize(QSize(90, 90))
        self.sairButton.setFont(font2)
        icon4 = QIcon()
        icon4.addFile(u":/Main/icons/sairB.png", QSize(), QIcon.Normal, QIcon.Off)
        self.sairButton.setIcon(icon4)
        self.sairButton.setIconSize(QSize(64, 64))
        self.sairButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_2.addWidget(self.sairButton)


        self.horizontalLayout.addWidget(self.frame_buttons)


        self.verticalLayout.addWidget(self.frame_settings)

        self.frame_claim = QFrame(Dialog)
        self.frame_claim.setObjectName(u"frame_claim")
        self.frame_claim.setMaximumSize(QSize(16777215, 30))
        self.frame_claim.setFrameShape(QFrame.StyledPanel)
        self.frame_claim.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_claim)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.line_3 = QFrame(self.frame_claim)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_2.addWidget(self.line_3)

        self.label_6 = QLabel(self.frame_claim)
        self.label_6.setObjectName(u"label_6")

        self.verticalLayout_2.addWidget(self.label_6)


        self.verticalLayout.addWidget(self.frame_claim)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"PyScratch", None))
        self.label_image.setText(QCoreApplication.translate("Dialog", u"Waiting the file", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Dialog", u"Settings", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"File:", None))
        self.arquivoButton.setText(QCoreApplication.translate("Dialog", u"...", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Time between pictures:", None))
        self.timeNum.setInputMask(QCoreApplication.translate("Dialog", u"999", None))
        self.timeNum.setText(QCoreApplication.translate("Dialog", u"15", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Number of files:", None))
        self.quantidadeLabel.setText(QCoreApplication.translate("Dialog", u"000", None))
        self.groupBox.setTitle(QCoreApplication.translate("Dialog", u"Status", None))
        self.statusLabel.setText(QCoreApplication.translate("Dialog", u"Holding", None))
        self.analisarButton.setText(QCoreApplication.translate("Dialog", u"Run", None))
        self.pararButton.setText(QCoreApplication.translate("Dialog", u"Stop", None))
        self.salvarButton.setText(QCoreApplication.translate("Dialog", u"Save", None))
        self.sairButton.setText(QCoreApplication.translate("Dialog", u"Exit", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Written and compiled by Vladimir Gaal - February 2021 - S\u00e3o Paulo, Brazil - Thanks to the icons8.com site for providing for free the icons used", None))
    # retranslateUi

