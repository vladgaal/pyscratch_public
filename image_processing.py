import numpy as np
import cv2
import process_contours as proc
import matplotlib.pyplot as plt
import datetime
import os
import copy
import check

sufixo_de_saida = '_resultado'
now = datetime.datetime.now()
font = cv2.FONT_HERSHEY_PLAIN
verde = (0, 255, 0)
vermelho = (0, 0, 255)
preto = (0, 0, 0)
azul = (255, 0, 0)
branco = (255, 255, 255)


def process_a_picture(path, nome, formato, corte):
    # carregando imagem
    dir = os.path.abspath(path + nome + formato)
    im = cv2.imread(dir)
    contours = proc.treatment(im, corte)
    height, width, channels = im.shape
    tamanho = height * width

    maior_area = 0
    maior_area_index = []
    continua = True
    while continua:
        for c in range(len(contours)):
            if cv2.contourArea(contours[c]) >= tamanho:
                print("aqui")
                maior_area = maior_area + (cv2.contourArea(contours[c]))
                maior_area_index.append(int(c))

        if len(maior_area_index) > 0:
            # desenhando na figura
            for a in range(len(maior_area_index)):
                cv2.drawContours(im, contours, maior_area_index[a], verde, 2)
                cv2.putText(im, '{}'.format(maior_area_index[a]), (int(contours[maior_area_index[a]][0][0][0]) - 50,
                                                                   int(contours[maior_area_index[a]][0][0][1]) + 25),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, vermelho, 2, cv2.LINE_AA)
                cv2.putText(im, 'File: {}{}'.format(nome, formato), (2, 32), cv2.FONT_HERSHEY_PLAIN, 1.5, preto, 1,
                            cv2.LINE_AA)
                cv2.putText(im, 'File: {}{}'.format(nome, formato), (0, 30), cv2.FONT_HERSHEY_PLAIN, 1.5, verde, 1,
                            cv2.LINE_AA)
                cv2.putText(im, 'Area: {}pixels'.format(maior_area), (2, 62), cv2.FONT_HERSHEY_PLAIN, 1.5, preto, 1,
                            cv2.LINE_AA)
                cv2.putText(im, 'Area: {}pixels'.format(maior_area), (0, 60), cv2.FONT_HERSHEY_PLAIN, 1.5, verde, 1,
                            cv2.LINE_AA)
                cv2.putText(im, 'Date: {}/{}/{}'.format(now.day, now.month, now.year), (2, 92), cv2.FONT_HERSHEY_PLAIN,
                            1.5,
                            preto, 1, cv2.LINE_AA)
                cv2.putText(im, 'Date: {}/{}/{}'.format(now.day, now.month, now.year), (0, 90), cv2.FONT_HERSHEY_PLAIN,
                            1.5,
                            verde, 1, cv2.LINE_AA)
                rect = cv2.minAreaRect(contours[maior_area_index[a]])
                box = cv2.boxPoints(rect)
                box = np.int0(box)
                cv2.drawContours(im, [box], 0, vermelho, 2)
            continua = False
        else:
            tamanho = tamanho - tamanho*0.05

        if tamanho < 2:
            maior_area = 0
            maior_area_index.append(0)
            continua = False

    cv2.imwrite('image.png', im)

    # Salvando imagem de saida
    path = path + '/Output'
    if not os.path.isdir(path):
        os.makedirs(path)
    filename = os.path.join(path, nome + '.tif')
    cv2.imwrite(filename, im)

    return maior_area


def process_a_picture2(path, nome, formato, corte, passImage=False):
    print(nome[-3:], end='')

    ## ==> LOADING IMAGE FILE
    dir = os.path.abspath(path + nome + formato)
    im = cv2.imread(dir)

    ## ==> GETTING CONTOURS
    contours = proc.treatment(im, corte)

    ## ==> SETTING PARAMETERS
    factor = 0.7
    maior_area = 0
    maior_area_index = []
    all_areas = []

    ## ==> CALCULATES THE AREA OF ALL CONTOURS
    for contour in contours:
        all_areas.append(cv2.contourArea(contour))

    ## ==> CHECK IF ANY AREA HAS BEEN FOUND
    if len(all_areas) == 0:
        print("->null", end='')
        ## ==> INSERTING LABELS AND PARAMETERS IN THE OUTPUT IMAGE
        cv2.putText(im, 'File: {}{}'.format(nome, formato), (2, 32), cv2.FONT_HERSHEY_PLAIN, 1.5, preto, 1, cv2.LINE_AA)
        cv2.putText(im, 'File: {}{}'.format(nome, formato), (0, 30), cv2.FONT_HERSHEY_PLAIN, 1.5, verde, 1, cv2.LINE_AA)
        cv2.putText(im, 'Area: {}pixels'.format(maior_area), (2, 62), cv2.FONT_HERSHEY_PLAIN, 1.5, preto, 1,
                    cv2.LINE_AA)
        cv2.putText(im, 'Area: {}pixels'.format(maior_area), (0, 60), cv2.FONT_HERSHEY_PLAIN, 1.5, verde, 1,
                    cv2.LINE_AA)
        cv2.putText(im, 'Date: {}/{}/{}'.format(now.day, now.month, now.year), (2, 92), cv2.FONT_HERSHEY_PLAIN, 1.5,
                    preto, 1, cv2.LINE_AA)
        cv2.putText(im, 'Date: {}/{}/{}'.format(now.day, now.month, now.year), (0, 90), cv2.FONT_HERSHEY_PLAIN, 1.5,
                    verde, 1, cv2.LINE_AA)

    else:
        ## ==> LARGE AREAS -> CONSIDER THE TOP 30%
        if max(all_areas) > 5000:
            limit = np.log(np.amax(all_areas)) * factor
            for index in range(len(all_areas)):
                if all_areas[index] >= np.exp(limit):
                    maior_area_index.append(index)

        ## ==> MID AREAS -> CONSIDER THE 4 BIGGEST VALUES
        elif max(all_areas) > 1000:
            all_sort = copy.deepcopy(all_areas)
            all_sort.sort()
            all_sort.reverse()
            for index in range(4 if len(all_sort) > 4 else len(all_sort)):
                for a in range(len(all_areas)):
                    if all_sort[index] == all_areas[a]:
                        maior_area_index.append(a)

        ## ==> SMALL AREAS -> CONSIDER THE BIGGEST VALUES
        else:
            all_sort = copy.deepcopy(all_areas)
            all_sort.sort()
            all_sort.reverse()
            for a in range(len(all_areas)):
                if all_sort[0] == all_areas[a]:
                    maior_area_index.append(a)

        ## ==> CHECK INTERSECTION AND SET TO REMOVE
        to_remove = []
        if len(maior_area_index) > 1:
            for i in maior_area_index:
                for j in maior_area_index:
                    if i != j:
                        if check.contourIntersect(im, contours[i], contours[j]):
                            if all_areas[i] < all_areas[j]:
                                if not i in to_remove:
                                    to_remove.append(i)
                                    #print(nome[-3:], i)
                            else:
                                if not j in to_remove:
                                    to_remove.append(j)
                                    #print(nome[-3:], j)

        ## ==> REMOVE INTERSECTING ITEMS
        for item in to_remove:
            maior_area_index.remove(item)

        ## ==> SUM OF THE BIGGEST AREAS
        for item in maior_area_index:
            maior_area = maior_area + all_areas[item]

        ## ==> INSERTING LABELS AND PARAMETERS IN THE OUTPUT IMAGE
        for indice_da_maior in maior_area_index:
            cv2.drawContours(im, contours, indice_da_maior, verde, 2)
            cv2.putText(im, '{}'.format(indice_da_maior), (int(contours[indice_da_maior][0][0][0]) - 50,
                                                           int(contours[indice_da_maior][0][0][1]) + 25),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, vermelho, 2, cv2.LINE_AA)
            cv2.putText(im, 'File: {}{}'.format(nome, formato), (2, 32), cv2.FONT_HERSHEY_PLAIN, 1.5, preto, 1, cv2.LINE_AA)
            cv2.putText(im, 'File: {}{}'.format(nome, formato), (0, 30), cv2.FONT_HERSHEY_PLAIN, 1.5, verde, 1, cv2.LINE_AA)
            cv2.putText(im, 'Area: {}pixels'.format(maior_area), (2, 62), cv2.FONT_HERSHEY_PLAIN, 1.5, preto, 1, cv2.LINE_AA)
            cv2.putText(im, 'Area: {}pixels'.format(maior_area), (0, 60), cv2.FONT_HERSHEY_PLAIN, 1.5, verde, 1, cv2.LINE_AA)
            cv2.putText(im, 'Date: {}/{}/{}'.format(now.day, now.month, now.year), (2, 92), cv2.FONT_HERSHEY_PLAIN, 1.5,
                        preto, 1, cv2.LINE_AA)
            cv2.putText(im, 'Date: {}/{}/{}'.format(now.day, now.month, now.year), (0, 90), cv2.FONT_HERSHEY_PLAIN, 1.5,
                        verde, 1, cv2.LINE_AA)
            rect = cv2.minAreaRect(contours[indice_da_maior])
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            cv2.drawContours(im, [box], 0, vermelho, 2)

    ## ==> SAVING THE IMAGE FOR GUI (OLD WAY)
    if not passImage:
        cv2.imwrite('image.png', im)

    ## ==> SAVING THE OUTPUT IMAGE
    path = path + '/Output'
    if not os.path.isdir(path):
        os.makedirs(path)
    filename = os.path.join(path, nome + '.tif')
    cv2.imwrite(filename, im)

    print("->", maior_area)
    if passImage:
        return maior_area, im
    else:
        return maior_area


if __name__ == '__main__':
    # inicialização de variáveis
    path = 'D:\\GDrive\\Fer\\170804_174239_Plate 1\\'
    nome = 'A1_03_1_1_Phase Contrast_'
    deltaT = []
    time = []
    areas = []
    numeros = []
    for a in range(1, 250):
        numeros.append(str(a+1).zfill(3))
    formato = '.tif'
    corte = 2
    for numero in numeros:
        a = datetime.datetime.now()
        area = process_a_picture2(path, nome+numero, formato, corte)
        b = datetime.datetime.now()
        c = b - a
        deltaT.append(c.microseconds/1000)
        time.append(a)
        areas.append(area)
        print(numero, area, c.microseconds/1000)

    print("Total time: ", time[-1] - time[0])

    fig = plt.figure()
    ax1 = plt.subplot(111)
    ax1.plot(numeros, deltaT, 'ro', label="Time lapse")
    ax1.set_ylabel("Time lapse [ms]")

    ax2 = ax1.twinx()
    ax2.plot(numeros, areas, 'bo', label="Area")
    ax2.set_ylabel('Area [$pixels^2$]')
    ax2.plot(numeros, [5000 for x in numeros], 'g-')
    ax2.plot(numeros, [1000 for x in numeros], 'g-')

    plt.legend(loc="best")
    plt.tight_layout()
    plt.show()

